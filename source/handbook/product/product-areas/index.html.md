---
layout: markdown_page
title: "Product Areas"
---

GitLab is one integrated product. But to facilitate communication and collaboration, we loosely categorize GitLab into these areas.

## Discussion

### Features

GitLab Discussion (GD) is focused on the collaboration features of GitLab, with major areas including:
* Code review, merge requests, and approvals
* Groups
* Issues
* Issue boards
* Labels
* Milestones
* Projects
* Snippets
* Wikis

GD does _not_ focus on these areas:
* CI/CD
* Cycle analytics
* GitLab integration (third-party services)

### Scenarios

Product Managers, Project Managers, Business Analysts, and Engineering Managers figure out what needs to be built based on investigation and analysis, which they do themselves and/or work with colleagues on. 
They translate these concepts into something one level more concrete. 

This is where GD begins.
GD provides the tools to enable these folks to document and collaborate on those concrete ideas.
These include issues, commenting, prioritization, and organization features.

UX Designers, Visual Designers, Software Engineers, Test Engineers, and Database Engineers create the product in a more tangible form.
GD allows these folks to collaborate with each other.
This creation is from ideation to shipping the product. 
Branching, merge requests, and code review are the main features supporting collaboration. 
The existing features are focused on the git system and the concept of source control.
The first wave of innovation is making these easier so that engineers don't need to be git experts and they can get a lot done in a browser context.
The next innovation is to jump out of the git paradigm and abstract away from commits and branching. 

GD allows all these folks to zoom out and look at the big picture, and zoom back in to focus in on tiny details, depending on the scenario.
We are working to strengthen this zoom in and zoom out dynamic, with milestones and boards being the early stages of this innovation.