---
layout: markdown_page
title: "Partner marketing"
---

Welcome to the Partner Marketing Handbook

[Up one level to the Product Marketing Handbook](/handbook/marketing/product-marketing/)    

## On this page
* [Partner Marketing](#partnermarketing)

## Partner Marketing Objectives<a name="partnermarketing"></a>

- Promote existing partnerships to be at top-of-mind for developers
- Integrate resale partnerships: promote partnership integrations/products which we sell as part of our sales process.
- Migrate open source projects to adopt GitLab, and convert their users in-turn to GitLab.
- Surface the ease of GitLab integration to encourage more companies to integrate with GitLab.
- Build closer relationships with existing partners through consistent communication

- [Strategic Partner listing](https://docs.google.com/document/d/1-oAf0tMlTrAaPAsG_8NLXrI3DEZqI5ZA0gW0lKxFjA4/edit) (internal)

## Partner Marketing Initiatives
* [Partner Newsletter Feature](#partnermarketing)

# Partner Newsletter Feature
In line with the objective of: Promote existing partnerships to be at top-of-mind for developers, a regular feature in our fortnightly (8th & 22nd) newsletter will promote our partners to our target audience.

Possible content:
- Feature on new partner if signed
- Feature on existing partner if major update released
- Feature on existing partner highlighting benefits of partner product
- 1-minute video showcasing the integration as a reference
- Blog post from partner
- Feature on how existing customer uses GitLab and partner

Suggested Format:
- Length: Couple of paragraphs
- Links: GitLab integration/partner page & partner website
- ROI - Track click-through to partner website and provide partner with metrics

Submit to the 8th and 22nd of the month newsletter issue in the marketing project